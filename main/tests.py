from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .models import Kegiatan, Orang
from .views import kegiatan, formorang
from .forms import Input_Form, Input_Form_Orang




@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class KegiatanUnitTestURL(TestCase):

    # Cek URL kegiatan (Halaman Utama)
    def test_apakah_ada_url_kegiatan(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    # Cek apakah path / fungsinya adalah kegiatan dan templatenya kegiatan.html
    def test_apakah_view_dan_template_dari_path_kegiatan(self):
        response = self.client.get('/')
        found = resolve('/')
        self.assertEqual(found.view_name, "main:kegiatan")
        self.assertTemplateUsed(response, 'main/kegiatan.html')

    # Cek apakah path baca kegiatan sudah terbuat
    def test_apakah_ada_url_bacakegiatan(self):
        response = Client().get('/bacakegiatan')
        self.assertTemplateNotUsed(response)
        self.assertEqual(response.status_code, 302)
        
class KegiatanUnitTestHTML(TestCase):
    #Cek Isi HTML Kegiatan yang ada di views kegiatan + apakah method get pada views kegiatan menampilkan form kosong
    def test_isi_html_kegiatan_khusus_formkegiatan(self):
        request = HttpRequest()
        response = kegiatan(request)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("Submit", html_kegiatan)
        self.assertIn("Nama Kegiatan:", html_kegiatan)

class KegiatanUnitTestModel(TestCase):
    #Cek Apakah Model Kegiatan Sudah Terbuat
    def test_apakah_model_kegiatan_sudah_terbuat(self):
        Kegiatan.objects.create(namaKegiatan="Youtube-ing")
        Kegiatan.objects.create(namaKegiatan="Cooking")
        jumlah_objek_kegiatan = Kegiatan.objects.all().count()
        self.assertEquals(jumlah_objek_kegiatan, 2)

    # Cek field dari models
    def test_apakah_nama_yang_dibuat_di_objek_benar(self):
        Kegiatan.objects.create(namaKegiatan="Youtube-ing")
        kegiatan = Kegiatan.objects.get(namaKegiatan="Youtube-ing")
        self.assertEqual(kegiatan.namaKegiatan, "Youtube-ing")
        self.assertEqual(kegiatan.id, 1)

class KegiatanUnitTestForm(TestCase):
    # Cek apakah form valid
    def test_apakah_post_form_valid(self):
        form = Input_Form(data={"namaKegiatan" : "Tidur"})
        self.assertTrue(form.is_valid)
    
    # Cek apakah objek terbuat saat ada isian form
    def test_apakah_objek_dapat_dibuat_dari_form(self):
        form = Input_Form(data={"namaKegiatan" : "Tidur"})
        form.save()
        jumlah_objek_dari_form = Kegiatan.objects.all().count()
        self.assertEqual(jumlah_objek_dari_form, 1)






class FormOrangUnitTestURL(TestCase):    
    # Cek URL form orang
    def test_apakah_ada_url_formorang(self):
        response = Client().get('/formorang')
        self.assertEqual(response.status_code, 200)

    # Cek apa view dan template dari path formorang
    def test_apakah_view_dan_template_dari_path_formorang(self):
        response = Client().get('/formorang')
        found = resolve('/formorang')
        self.assertEqual(found.view_name, "main:formorang")
        self.assertTemplateUsed(response, 'main/formorang.html')

class FormOrangUnitTestModel(TestCase):
    # Cek Apakah Model Orang Sudah Terbuat
    def test_apakah_model_orang_sudah_terbuat(self):
        kegiatan = Kegiatan.objects.create(namaKegiatan="Tidur")
        Orang.objects.create(kegiatanOrang=kegiatan, namaOrang="Pewe")
        jumlah_objek_orang = Orang.objects.all().count()
        self.assertEquals(jumlah_objek_orang, 1)

    # Cek field dari models
    def test_apakah_nama_yang_dibuat_di_objek_benar(self):
        kegiatan = Kegiatan.objects.create(namaKegiatan="Youtube-ing")
        Orang.objects.create(kegiatanOrang=kegiatan, namaOrang="Pewe")
        kegiatanHasil = Orang.objects.get(kegiatanOrang=kegiatan)
        self.assertEqual(kegiatanHasil.namaOrang, "Pewe")


class FormOrangUnitTestHTML(TestCase):
    #Cek Isi HTML Kegiatan yang ada di views formorang + apakah method get pada views kegiatan menampilkan form kosong
    def test_isi_html_formorang(self):
        request = HttpRequest()
        response = formorang(request)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("Submit", html_kegiatan)
        self.assertIn("Nama Orang:", html_kegiatan)
        
class UnitTestForm(TestCase):
    # Cek apakah form valid
    def test_apakah_post_form_valid(self):
        form = Input_Form_Orang(data={"namaOrang" : "Icha"})
        self.assertTrue(form.is_valid)


