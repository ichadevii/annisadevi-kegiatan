from django import forms
from .models import Kegiatan, Orang

class Input_Form(forms.ModelForm):
        class Meta:
            model = Kegiatan
            fields = ['namaKegiatan']

        error_messages={
            'required' : 'Please Type'
        }

        kegiatan_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : 'Tidur'
        }

        namaKegiatan = forms.CharField(label='Nama Kegiatan', required=True, max_length=30, widget=forms.TextInput(attrs=kegiatan_attrs))


class Input_Form_Orang(forms.ModelForm):
        class Meta:
            model = Orang
            fields = ['namaOrang', 'kegiatanOrang']

        error_messages={
            'required' : 'Please Type'
        }

        orang_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : 'Pewe'
        }

        namaOrang = forms.CharField(label='Nama Orang:', required=True, max_length=30, widget=forms.TextInput(attrs=orang_attrs))
        
    