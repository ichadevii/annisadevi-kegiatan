from django.urls import path

from .views import kegiatan, formorang, bacakegiatan

app_name = 'main'

urlpatterns = [
    path('', kegiatan, name='kegiatan'),
    path('formorang', formorang, name='formorang'),
    path ('bacakegiatan', bacakegiatan, name="bacakegiatan"),
]
