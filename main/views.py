from django.shortcuts import render, redirect
from .models import Kegiatan, Orang
from .forms import Input_Form, Input_Form_Orang

response = {}
def kegiatan(request):
    form = Input_Form(request.POST or None)
    if request.method=="POST" and form.is_valid:
        form.save()
        return redirect('/bacakegiatan')
    else:
        form = Input_Form()
        response['form'] = form

        kegiatan=Kegiatan.objects.all()
        response['kegiatan'] = kegiatan

        orang = Orang.objects.all()
        response['orang'] = orang

        return render(request, 'main/kegiatan.html', response)

def bacakegiatan(request):
    return redirect('/')


def formorang(request):
    form2 = Input_Form_Orang(request.POST or None)
    if request.method=="POST" and form2.is_valid:
        form2.save()
        return redirect('/bacakegiatan')
    else:
        form2 = Input_Form_Orang()
        response['form2'] = form2
        return render(request, 'main/formorang.html', response)
