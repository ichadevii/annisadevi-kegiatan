from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=30)
    
class Orang(models.Model):
    namaOrang = models.CharField(max_length=30)
    kegiatanOrang = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, default=0)